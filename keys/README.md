# Erzeugen der für diesen Test verwendeten Keys

Es werden Dummy Keys erzeugt und angelegt

```` bash
./create_keys.sh
````

Dieses Skript erzeugt die gpg-keys für den Sender und den Empfänger.

```` bash
./create_mails.sh
````

Dieses Skript erzeugt die passenden Mail-Formate, und benutzt dazu WriteMail. Das ist ein Testskript.

```` bash
./read_mails.sh
````

Dieses Skript liest alle erzeugten Emails und dekodiert sie wieder zurück. Das ist ein Testskript.