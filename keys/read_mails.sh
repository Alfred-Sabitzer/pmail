#!/bin/bash
#
# Lesen der Emails
#
#shopt -o -s errexit	#—Terminates  the shell	script if a	command	returns	an error code.
shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
shopt -o -s nounset	#-No Variables without definition
# Allgemmeine Definitionen
source ../test_mail/test_mail_environment.sh
#
../venv/bin/python ../ReadMail.py --idir="../test_mail/test" --odir="../test_mail/test_read/test_output" --pgppw=" "
#