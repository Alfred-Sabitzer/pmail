#!/bin/bash
#
# Erzeugen der temporären Keys
#
#shopt -o -s errexit	#—Terminates  the shell	script if a	command	returns	an error code.
shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
shopt -o -s nounset	#-No Variables without definition
# Allgemmeine Definitionen
source ../test_mail/test_mail_environment.sh

# Löschen und neu erzeugen der pgp-keys
function keymgmt ()
{
  my_tmp_key="${1}"
  # Erst wird der Key gelöscht
  key_is_here="--$(gpg --list-keys | grep -i ${my_tmp_key})"
  if [ "${key_is_here}" == '--' ]; then
    echo "Key does not exist:${key_is_here}"
  else
    echo "delete key"
    my_fingerprint=$(gpg --fingerprint ${my_tmp_key} | grep -v pub | grep -v uid)
    gpg --batch --yes --delete-secret-key "${my_fingerprint}"
    gpg --batch --yes --delete-key ${my_tmp_key}
  fi
  # Neuer key wird erzeugt
  cat <<EOF | gpg --batch --yes --verbose --generate-key
       %echo Generating a basic OpenPGP key
       Key-Type: RSA
       Key-Length: 4096
       Name-Real: Temporary Email for ${my_tmp_key}
       Name-Comment: with stupid passphrase
       Name-Email: ${my_tmp_key}
       %no-protection
       Expire-Date: 0
       # Do a commit here, so that we can later print "done" :-)
       %commit
       %echo done
EOF
  # Key wird exportiert
  gpg --output ./${my_tmp_key}_public.asc --batch --yes --armor --export ${my_tmp_key}
  gpg --output ./${my_tmp_key}_private.asc --batch --yes  --armor --export-secret-key ${my_tmp_key}
}
#
keymgmt "${my_sender_key}"
keymgmt "${my_recipient_key}"
#