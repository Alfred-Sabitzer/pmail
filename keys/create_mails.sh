#!/bin/bash
#
# Erzeugen der Emails
#
#shopt -o -s errexit	#—Terminates  the shell	script if a	command	returns	an error code.
shopt -o -s xtrace	#—Displays each	command	before it’s	executed.
shopt -o -s nounset	#-No Variables without definition
# Allgemmeine Definitionen
source ../test_mail/test_mail_environment.sh
#
function mailmgmt ()
{
  my_idir="${1}"
  my_ofile="${2}"
  my_pw="${3}"
  cat <<EOF > ${my_idir}header.txt
Subject: Das ist ${my_idir}
From: sender <${my_sender_key}>
To: recipient <${my_recipient_key}>
EOF
  ../venv/bin/python ../WriteMail.py --idir=${my_idir} --ofile=${my_ofile} --pgppw="${my_pw}"
}

#
mailmgmt "../test_mail/test_mail_plain/" "../test_mail/test/test_mail_plain.eml" ""
mailmgmt "../test_mail/test_mail_html/" "../test_mail/test/test_mail_html.eml" ""
mailmgmt "../test_mail/test_mail_pgp/" "../test_mail/test/test_mail_pgp.eml" ""
mailmgmt "../test_mail/test_mail_pgp_simple/" "../test_mail/test/test_mail_pgp_simple.eml" ""
mailmgmt "../test_mail/test_mail_UTF8/" "../test_mail/test/test_mail_UTF8.eml" ""
#