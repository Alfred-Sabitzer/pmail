#!/bin/bash
############################################################################################
# https://www.geeksforgeeks.org/using-curl-to-send-email/
############################################################################################
shopt -o -s errexit #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
 
mySendMail=$(find /  | grep -i SendMail.py)
python3 $mySendMail --MAIL_SMTP_HOST=${MAIL_SMTP_HOST} \
      --MAIL_USER=${MAIL_USER} \
      --MAIL_PASSWORD=${MAIL_PASSWORD} \
      --MAIL_FILE=../test_mail/test/test_mail_pgp_simple.eml 
 
