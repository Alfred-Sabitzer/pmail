# Daten und Testegebnisse

Hier liegen die Testdaten für WriteMail.

```` bash
$ tree
.
├── README.md
├── test
│   ├── README.md
│   ├── test_mail_html.eml
│   ├── test_mail_html.eml.sh
│   ├── test_mail_pgp.eml
│   ├── test_mail_pgp.eml.sh
│   ├── test_mail_pgp_simple.eml
│   ├── test_mail_pgp_simple.eml.sh
│   ├── test_mail_plain.eml
│   ├── test_mail_plain.eml.sh
│   ├── test_mail_UTF8.eml
│   └── test_mail_UTF8.eml.sh
├── test_mail_environment.sh
├── test_mail_html
│   ├── body.html
│   ├── body.odt
│   ├── body.txt
│   └── header.txt
├── test_mail_pgp
│   ├── body.html
│   ├── body.odt
│   ├── body.pdf
│   ├── body.pgp
│   ├── body.txt
│   └── header.txt
├── test_mail_pgp_simple
│   ├── body.html
│   ├── body.pgp
│   ├── body.txt
│   └── header.txt
├── test_mail_plain
│   ├── body.html
│   ├── body.txt
│   └── header.txt
├── test_mail_UTF8
│   ├── body.html
│   ├── body.txt
│   └── header.txt
└── test_read
    ├── README.md
    ├── test.eml
    └── test_output
        ├── test.eml_attachments
        │   ├── autocrypt.txt
        │   ├── body.html
        │   ├── body.txt
        │   ├── dkim_signature.txt
        │   ├── header.txt
        │   ├── PGP.eml
        │   ├── PGP.eml_attachments
        │   │   ├── body.html
        │   │   ├── body.txt
        │   │   ├── header.txt
        │   │   └── OpenPGP_0x31054A50311130DC.asc
        │   ├── PGP.pgp
        │   └── PGP.txt
        ├── test_mail_html.eml_attachments
        │   ├── autocrypt.txt
        │   ├── body.html
        │   ├── body.odt
        │   ├── body.txt
        │   └── header.txt
        ├── test_mail_pgp.eml_attachments
        │   ├── autocrypt.txt
        │   ├── body.html
        │   ├── body.txt
        │   ├── header.txt
        │   ├── PGP.eml
        │   ├── PGP.eml_attachments
        │   │   ├── body.html
        │   │   ├── body.odt
        │   │   ├── body.pdf
        │   │   ├── body.txt
        │   │   └── header.txt
        │   ├── PGP.pgp
        │   └── PGP.txt
        ├── test_mail_pgp_simple.eml_attachments
        │   ├── autocrypt.txt
        │   ├── body.html
        │   ├── body.txt
        │   ├── header.txt
        │   ├── PGP.eml
        │   ├── PGP.eml_attachments
        │   │   ├── body.html
        │   │   ├── body.txt
        │   │   └── header.txt
        │   ├── PGP.pgp
        │   └── PGP.txt
        ├── test_mail_plain.eml_attachments
        │   ├── autocrypt.txt
        │   ├── body.html
        │   ├── body.txt
        │   └── header.txt
        └── test_mail_UTF8.eml_attachments
            ├── autocrypt.txt
            ├── body.html
            ├── body.txt
            └── header.txt
````

In den einzelnen Ordnern ist der Input für die zu erzeugenden Mails abgelegt.

```` bash
../keys/create_mails.sh
````

Dieses Skript erzeugt die passenden Mail-Formate im Verzeichnis ./test, und benutzt dazu WriteMail.

```` bash
../keys/read_mails.sh
````

Dieses Skript liest die Mails aus dem Verzeichnis ./test, und benutzt dazu ReadMail.