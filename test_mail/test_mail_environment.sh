#!/bin/bash
############################################################################################
# Environmentsettings.
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
#shopt -o -s nounset #-No Variables without definition
my_sender_key="my_sender@slainte.at"
my_recipient_key="my_recipient@slainte.at"
###