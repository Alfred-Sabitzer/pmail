# Daten und Testegebnisse

Hier liegen die Ergebnisse für ReadMail.

```` bash
$ tree

.
├── README.md
├── test.eml
└── test_output
    ├── test.eml_attachments
    │   ├── body.html
    │   ├── body.txt
    │   ├── header.txt
    │   ├── PGP.eml
    │   ├── PGP.eml_attachments
    │   │   ├── body.html
    │   │   ├── body.txt
    │   │   ├── header.txt
    │   │   └── OpenPGP_0x31054A50311130DC.asc
    │   ├── PGP.pgp
    │   └── PGP.txt
    ├── test_mail_html.eml_attachments
    │   ├── body.html
    │   ├── body.odt
    │   ├── body.txt
    │   └── header.txt
    ├── test_mail_pgp.eml_attachments
    │   ├── body.html
    │   ├── body.txt
    │   ├── header.txt
    │   ├── PGP.eml
    │   ├── PGP.eml_attachments
    │   │   ├── body.html
    │   │   ├── body.odt
    │   │   ├── body.pdf
    │   │   ├── body.txt
    │   │   └── header.txt
    │   ├── PGP.pgp
    │   └── PGP.txt
    ├── test_mail_pgp_simple.eml_attachments
    │   ├── body.html
    │   ├── body.txt
    │   ├── header.txt
    │   ├── PGP.eml
    │   ├── PGP.eml_attachments
    │   │   ├── body.html
    │   │   ├── body.txt
    │   │   └── header.txt
    │   ├── PGP.pgp
    │   └── PGP.txt
    ├── test_mail_plain.eml_attachments
    │   ├── body.html
    │   ├── body.txt
    │   └── header.txt
    └── test_mail_UTF8.eml_attachments
        ├── body.html
        ├── body.txt
        └── header.txt

````

In den einzelnen Ordnern ist der Output der gelesenen Mails zu finden.

```` bash
../keys/read_mails.sh
````

Dieses Skript liest die EML-Files und benutzt dazu ReadMail.