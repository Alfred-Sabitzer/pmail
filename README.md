# Mails Lesen, Schreiben und versenden

Diese Programme helfen beim Decodieren von Mail-Inhalten, bzw. beim Erzeugen versandbereiter Mails.
Die versandberetien Mails können auch über secure smtp (Port 587) versandt werden.
Generelle sind diese Programme zur Verwendugn im gescripteten Batch-Betrieb gedacht.

## Read-Mail 

liest alle *.eml Files in einem bestimmten Verzeichnis und decodiert sie.

```bash
#!/bin/bash
#
# Test  ReadMail
#
../venv/bin/python ../ReadMail.py --idir=./test_mail/test --odir=./test_mail/test_result --pgppw=aIQZknSAD7xNPQAjrYDqIXen3rUKZQ --secretkey=/home/alfred/Downloads/k8s-roboter-secret.asc --publickey=/home/alfred/Downloads/alfred-public.asc
#
```
Erzeugte Komponenten pro EML-File sind:

* header.txt  - Sender und Empfänger
* body.txt    - Einfache Nachricht
* alle vorhandenen Attachments werden ausgegeben.

## WriteMail

nimmt alle Komponenten aus einem Verzeichnis und erzeugt ein versendbares eml.

```bash
#!/bin/bash
#
# Test  WriteMail
#
../venv/bin/python ../WriteMail.py --idir="./test_mail/test_mail_plain/" --ofile="./test_mail/test/test_mail_plain.eml" --pgppw= "
```

Gelesene Komponenten sind:

* header.txt  - Sender und Empfänger
* body.txt    - Einfache Nachricht
* body.html   - Nachricht im HTML-Format
* alle vorhandenen Dateien werden als Attachments hinzugefügt
* body.pgp    - Dieses File muß einfach nur da sein. Dann wird alles im Ordner ein eine Mail verpackt, die dann verschlüsselt wird.

## SendMail

Versendet eine Email mit Hilfe der smtplib.

```bash
#!/bin/bash
#
# Test  SendMail
#
../venv/bin/python ../SendMail.py --MAIL_SMTP_HOST=mymailhost.at --MAIL_USER=mymailuser@mydomain.at --MAIL_PASSWORD=mypasswordtologin --MAIL_FILE=/FQFNtomymal/test_mail.eml
```

https://nvd.nist.gov/vuln/detail/CVE-2023-51764 Aufgrund dieser CVE akzeptiert postfix keine korrekt abgeschlossenen Emails 

```bash
* TLSv1.2 (IN), TLS header, Supplemental data (23):
  { [5 bytes data]
  < 521 5.5.2 mailcow.slainte.at Error: bare <LF> received
  100    75    0     0    0    75      0     29 --:--:--  0:00:02 --:--:--    29
* Connection #0 to host mailcow.slainte.at left intact
  curl: (56) Failure when receiving data from the peer
```

Es gibt zum heutigen Zeitpunkt leider noch keinen Fix für curl, darum ist dieses python Programm notwendig.

## Erzeugen des requirements.txt

Um die Abhängigkeiten zu dokumentieren (diese Datei kann dann in anderen Projekten verwendet werden) erzeugen wir das requrements.txt

```bash
source ./venv/bin/activate
pip3 freeze > requirements.txt
# install
pip3 install pipreqs

# Run in current directory
python3 -m  pipreqs.pipreqs .
```
## gnupg in Python

Um das Python-file auch in alpine-Images verwenden zu können, habe ich gpg jetzt über das OS-System in Verwendung. Es ist mir nicht gelungen, alle Abhängigkeiten im Image
"docker pull python:3.13.0b1-alpine3.19" zu installiereen.

Have FUN
