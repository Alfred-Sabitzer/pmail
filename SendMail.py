###########################################################################################################################################
#
# https://support.plesk.com/hc/en-us/articles/20970581221527-Unable-to-send-emails-using-PHP-Scripts-bare-LF-received-after-DATA
#
# https://nvd.nist.gov/vuln/detail/CVE-2023-51764
#
# Sending Emails with Curl does not work anymore in combination with mailcow
# No Fix avalable at the moment
###########################################################################################################################################
import smtplib
import sys
import getopt

def main(argv):
    MAIL_SMTP_HOST=""
    MAIL_USER=""
    MAIL_RECIPIENT=""
    MAIL_PASSWORD=""
    MAIL_FILE=""
    opts, args = getopt.getopt(argv,"h:m:u:r:p:f",["help","MAIL_SMTP_HOST=","MAIL_USER=","MAIL_RECIPIENT=","MAIL_PASSWORD=","MAIL_FILE="])
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print("Usage: --MAIL_SMTP_HOST=Hostname --MAIL_USER=Username --MAIL_RECIPIENT=recipientname --MAIL_PASSWORD=password --MAIL_FILE=FQFN")
            sys.exit()
        elif opt in ("-m", "--MAIL_SMTP_HOST"):
            MAIL_SMTP_HOST = arg
        elif opt in ("-u", "--MAIL_USER"):
            MAIL_USER = arg
        elif opt in ("-r", "--MAIL_RECIPIENT"):
            MAIL_RECIPIENT = arg
        elif opt in ("-p", "--MAIL_PASSWORD"):
            MAIL_PASSWORD = arg
        elif opt in ("-f", "--MAIL_FILE"):
            MAIL_FILE = arg

    # creates SMTP session
    s = smtplib.SMTP(MAIL_SMTP_HOST, 587)
    # start TLS for security
    s.starttls()
    # Authentication
    s.login(MAIL_USER, MAIL_PASSWORD)
    # message to be sent
    text_file = open(MAIL_FILE, 'r')
    message = text_file.read()
    text_file.close()
    # sending the mail
    s.sendmail(MAIL_USER, MAIL_RECIPIENT, message)
    # terminating the session
    s.quit()

if __name__ == "__main__":
    if len(sys.argv) > 2:
        main(sys.argv[1:])
    else:
        print("Usage: --MAIL_SMTP_HOST=Hostname --MAIL_USER=Username --MAIL_RECIPIENT=recipientname --MAIL_PASSWORD=password --MAIL_FILE=FQFN")