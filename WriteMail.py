#!/usr/bin/env python3
#
# Bauen einer versendbaren Mail
#
# Es gibt die folgenden Dateien:
#   header.txt  - Empfänger und Absender und Subject
#
#   body.txt    - Einfache Nachricht
#   body.html   - Nachricht im HTML-Format
#   alle vorhandenen Attachments werden hinzugefügt.
#
#   body.pgp    - Indikator, das verschlüsselt werden muß
#
#
import getopt
import os
#import gnupg  --  Macht Probleme im Alpine-Image
import sys
import mimetypes

from email.header import Header
from email.mime.application import MIMEApplication
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from email import utils
from email.message import EmailMessage

# Debug to File
def do_output(msg):
    with open('/tmp/mailbody.txt', 'wb') as f:
        f.write(bytes(msg))
    return 0

# Alternative Encoding
def alternative(data, contenttype, charset):
    maintype, subtype = contenttype.split('/')
    if maintype == 'text':
        retval = MIMEText(data, _subtype=subtype, _charset=charset)
    else:
        retval = MIMEBase(maintype, subtype)
        retval.set_payload(data)
        encoders.encode_base64(retval)
    return retval

# Handle Attachments
def attachment(path,filename):
    mimetype, mimeencoding = mimetypes.guess_type(path+filename)
    if mimeencoding or (mimetype is None):
        mimetype = 'application/octet-stream'
    maintype, subtype = mimetype.split('/')
    if maintype == 'text':
        fd = open(path+filename, 'r')
        retval = MIMEText(fd.read(), _subtype=subtype)
    else:
        fd = open(path+filename, 'rb')
        retval = MIMEBase(maintype, subtype)
        retval.set_payload(fd.read())
        encoders.encode_base64(retval)

    retval.add_header('Content-Disposition', 'attachment',
                      filename = filename)
    fd.close()
    return retval

# Construct Message Structure
def compose_msg(msg,path):
    if os.stat(path+'body.html').st_size != 0:
        # Add the html version.  This converts the message into a multipart/alternative
        # container, with the original text message as the first part and the new html
        # message as the second part.
        msg2 = MIMEMultipart('alternative')
        text_file = open(path + 'body.txt', 'r')
        data = text_file.read()
        text_file.close()
        charset='none'
        if data.isascii():
            charset='us-ascii'
        else:
            charset='UTF-8'
        msg2.attach(alternative(data, 'text/plain', charset))
        text_file = open(path + 'body.html', 'r')
        data = text_file.read()
        text_file.close()
        charset='none'
        if data.isascii():
            charset='us-ascii'
        else:
            charset='UTF-8'
        msg2.attach(alternative(data, 'text/html', charset))
        msg.attach(msg2)
    else:
        if os.stat(path+'body.txt').st_size != 0:               # Entweder Body oder HTML
            text_file = open(path + 'body.txt', 'r')
            data = text_file.read()
            text_file.close()
            if data.isascii():
                body = MIMEText( _text=data, _subtype='plain', _charset='us-ascii')
            else:
                body = MIMEText(data, _subtype='plain', _charset='UTF-8')
            msg.attach(body)

    listing = os.listdir(path)
    for fle in listing:
        # do_output(msg)
        if fle != "header.txt":
            if fle != "body.txt":
                if fle != "body.html":
                    if fle != "body.pgp":
                        msg.attach(attachment(path,fle))

def main(argv):
    opts, args = getopt.getopt(argv,"h:i:o:p",["help","idir=","ofile=", "pgppw="])
    outfile=""
    path=""
    pgppw="#"
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print("Usage: --idir=<Verzeichnis mit Input-Dateien> --ofile=<FQFN für die Ausgabedatei> --pgppw=<Passwort des pgp-keys>")
            sys.exit()
        elif opt in ("-i", "--idir"):
            path = arg
        elif opt in ("-o", "--ofile"):
            outfile = arg
        elif opt in ("-o", "--pgppw"):
            pgppw = arg

    # Create the base text message.
    sendermail="echo@slainte.at"
    xsubject="..."
    message_id="NoMessageIDFound"
    toPureMail="NoPureMailFound"
    tomail="NoToMailFound"

    file1 = open(path + 'header.txt', 'r')
    for line in file1:
        xl=line.strip()
        result = xl.find('To:')
        if result == 0:
            tomail= xl[4:]
            toPureMail=tomail
            result = toPureMail.find('<')
            if result >=0:
                toPureMail= toPureMail[result+1:]
            result = toPureMail.find('>')
            if result >0:
                toPureMail= toPureMail[:result]
        result = xl.find('From:')
        if result == 0:
            result = xl.find('@')
            domainname=xl[result+1:]
            message_id=utils.make_msgid(domain=domainname)

            sendermail= xl[6:]
            result = sendermail.find('<')
            if result >=0:
                sendermail= sendermail[result+1:]
            result = sendermail.find('>')
            if result >0:
                sendermail= sendermail[:result]
        result = xl.find('Subject:')
        if result == 0:
            xsubject=xl[8:]
            # print("xsubject="+xsubject)
    file1.close()

    tomail=tomail.strip()
    sendermail=sendermail.strip()

    isPGP = os.path.exists(path +"body.pgp")
    if isPGP:
        pgpmsg = MIMEBase(_maintype="multipart", _subtype="mixed", protected_headers="v1")
        pgpmsg["Subject"]=Header(xsubject, 'utf-8')
        pgpmsg["X-Author"] = 'https://gitlab.com/Alfred-Sabitzer/pmail'
        #do_output(pgpmsg)
        compose_msg(pgpmsg,path)
        #do_output(pgpmsg)

        # gpg = gnupg.GPG()
        # gpg.encoding = 'utf-8'
        with open('/tmp/mailbody.txt', 'wb') as f:
            f.write(bytes(pgpmsg))
        os.system("echo "+pgppw+" | gpg --batch --yes --passphrase-fd 0 --encrypt --sign --armor --trust-model always --recipient "+toPureMail+" --local-user "+sendermail+" --output "+outfile+" /tmp/mailbody.txt")
        # encrypted = gpg.encrypt(data=bytes('\n', encoding='utf8')+bytes(pgpmsg), recipients=toPureMail, always_trust=True, sign=sendermail, passphrase=pgppw, armor=True)
        with open(outfile, 'r') as f:
            data=f.read()
        # do_output(data)
        xsubject="..."
        # https://datatracker.ietf.org/doc/html/draft-autocrypt-lamps-protected-headers-02
        # PGP/MIME encrypted message as described by https://de.wikipedia.org/wiki/PGP/MIME
        message = MIMEMultipart(_subtype="encrypted", protocol="application/pgp-encrypted")#
        #message.add_header("\nThis is an OpenPGP/MIME encrypted message (RFC 4880 and 3156))\n","")
        msg_desc = MIMEApplication(_data="Version: 1\n", _subtype="pgp-encrypted", _encoder=encoders.encode_noop)
        msg_desc.add_header("Content-Description", "PGP/MIME version identification")
        msg_enc = MIMEApplication(_data=data, _subtype='octet-stream; name="encrypted.asc"', _encoder=encoders.encode_noop)
        msg_enc.add_header("Content-Description", "OpenPGP encrypted message")
        msg_enc.add_header("Content-Disposition", 'inline; filename="encrypted.asc"')
        msg_enc.set_payload(data)
        message.attach(msg_desc)
        message.attach(msg_enc)
    else:
        message = MIMEMultipart("mixed");
        compose_msg(message, path)

    # Make a local copy of what we are going to send.
    with open(outfile, 'wb') as f:
        # Add Standard headers
        f.write(bytes("MIME-Version: 1.0\nUser-Agent: WriteMail.py\nContent-Language: en-US, de-AT-frami\n", encoding='utf8'))
        f.write(bytes("From: "+sendermail+"\n", encoding='utf8'))
        f.write(bytes("To: "+tomail+"\n", encoding='utf8'))
        f.write(bytes("Message-ID: "+message_id+"\n", encoding='utf8'))
        f.write(bytes("Date: "+utils.formatdate(localtime = True)+"\n", encoding='utf8'))
        f.write(bytes("X-Author: https://gitlab.com/Alfred-Sabitzer/pmail"+"\n", encoding='utf8'))
        f.write(bytes("Subject: "+xsubject+"\n", encoding='utf8'))
        f.write(bytes(message))

    # CommandFile for sending
    with open(outfile+".sh", 'w') as f:
        f.writelines("#!/bin/bash\n")
        f.writelines("############################################################################################\n")
        f.writelines("# https://www.geeksforgeeks.org/using-curl-to-send-email/\n")
        f.writelines("############################################################################################\n")
        f.writelines("shopt -o -s errexit #—Terminates  the shell script  if a command returns an error code.\n")
        f.writelines("shopt -o -s xtrace #—Displays each command before it’s executed.\n")
        f.writelines("shopt -o -s nounset #-No Variables without definition\n")
        f.writelines(" \n")
        f.writelines("mySendMail=$(find /  | grep -i SendMail.py)\n")
        f.writelines("python3 $mySendMail --MAIL_SMTP_HOST=${MAIL_SMTP_HOST} \\\n")
        f.writelines("      --MAIL_USER=${MAIL_USER} \\\n")
        f.writelines("      --MAIL_PASSWORD=${MAIL_PASSWORD} \\\n")
        f.writelines("      --MAIL_FILE="+outfile+" \n")
        f.writelines(" \n")

if __name__ == "__main__":
    if len(sys.argv) > 2:
        main(sys.argv[1:])
    else:
        print("Usage: --idir=<Verzeichnis mit Input-Dateien> --ofile=<FQFN für die Ausgabedatei> --pgppw=<Passwort für pgp")