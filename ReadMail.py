#
# Zerlegt eine Mail-Datei in ihre Bestandteile
#
# Wenn PGP-Mail, dann muss das PGP-File entschlüsselt werden.
# Danach wird eine künstliche Mail-Datei gemacht, die nochmals zerlegt wird.
#
# Ergebnisse sind immer im
#   body.html
#   body.txt            -> Hier sollte eigentlich die Nachricht drinnen sein.
#   header.txt          -> Hier findet man die Sender-Adresse und das Subject (wenn nicht verschlüsselt).
#
# Daneben "liegen" ein paar Attachments.
#

import os
# Not supported in Alpine
#import gnupg
import email
import sys
import getopt
#import subprocess


from email.header import decode_header, make_header
# from email.mime.multipart import MIMEMultipart
# from email.mime.text import MIMEText
# from email import utils

#
# Aufsplitten der Mail
#
def split_mail(path,outpath,fle):
    os.system("rm -rf "+outpath+"/"+fle+"_attachments/")
    os.mkdir(outpath+"/"+fle+"_attachments/")
    BodyFile = open(outpath +"/"+fle+"_attachments/body.txt", 'w')
    HTMLFile = open(outpath +"/"+fle+"_attachments/body.html", 'w')
    HeaderFile = open(outpath +"/"+fle+"_attachments/header.txt", 'w')
    if str.lower(fle[-3:])=="eml":
        msg = email.message_from_file(open(path +"/"+ fle))
        attachments=msg.get_payload()
        # decode the email subject
        subject, encoding = email.header.decode_header(str(msg.get('subject')))[0]
        if isinstance(subject, bytes):
            # if it is bytes, decode to str
            subject = subject.decode(encoding)
        print("Subject:", subject, file = HeaderFile)
        # decode email sender
        From, encoding = email.header.decode_header(msg.get('from'))[0]
        if isinstance(From, bytes):
            From = From.decode(encoding)
        print("From:", From, file = HeaderFile)
        # decode email recipient
        To, encoding = email.header.decode_header(msg.get('to'))[0]
        if isinstance(To, bytes):
            To = To.decode(encoding)
        print("To:", To, file = HeaderFile)
        # Check Autocrypt:
        Autocrypt = msg.get('Autocrypt')
        if Autocrypt is not None:
            if isinstance(Autocrypt, bytes):
                Autocrypt = Autocrypt.decode(encoding)
            AutocryptFile = open(outpath +"/"+fle+"_attachments/autocrypt.txt", 'w')
            print("Autocrypt:", Autocrypt, file = AutocryptFile)
            AutocryptFile.close()
        # Check DKIM-Signature:
        DKIM_Signature = msg.get('DKIM-Signature')
        if DKIM_Signature is not None:
            if isinstance(DKIM_Signature, bytes):
                DKIM_Signature = DKIM_Signature.decode(encoding)
            DKIM_SignatureFile = open(outpath +"/"+fle+"_attachments/dkim_signature.txt", 'w')
            print("DKIM-Signature:", DKIM_Signature, file = DKIM_SignatureFile)
            DKIM_SignatureFile.close()

        # todo
        # Check Spam-Info

        # if the email message is multipart
        if msg.is_multipart():
            # iterate over email parts
            for part in msg.walk():
                # extract content type of email
                content_type = part.get_content_type()
                content_disposition = str(part.get("Content-Disposition"))
                if content_type == "text/plain" and "attachment" not in content_disposition:
                    # print text/plain emails and skip attachments
                    body=""
                    try:
                        # get the email body
                        body = part.get_payload(decode=True).decode()
                    except:
                        pass
                    print(body, file = BodyFile)
                elif content_type == "text/html" and "attachment" not in content_disposition:
                    # print text/plain emails and skip attachments
                    body=""
                    try:
                        # get the email body
                        body = part.get_payload(decode=True).decode()
                    except:
                        pass
                    print(body, file = HTMLFile)
                elif content_type == "application/octet-stream" and "attachment" not in content_disposition:
                    # print text/plain emails and skip attachments
                    body=""
                    try:
                        # get the email body
                        body = part.get_payload(decode=True).decode()
                    except:
                        pass
                    # Kann nur einen geben!
                    PGPFile = open(outpath +"/"+fle+"_attachments/PGP.pgp", 'w')
                    print(body, file = PGPFile)
                    PGPFile.close()
                elif "attachment" in content_disposition:
                    # download attachment
                    filename = part.get_filename()
                    if filename:
                        folder_name = outpath +"/"+fle+"_attachments"
                        if not os.path.isdir(folder_name):
                            # make a folder for this email (named after the subject)
                            os.mkdir(folder_name)
                        filepath = os.path.join(folder_name, filename)
                        # download attachment and save it
                        open(filepath, "wb").write(part.get_payload(decode=True))
                elif content_type == "multipart/mixed" and content_disposition == "None": # Hier sind vielleicht die Protected headers drinnen (oder auch nicht).
                    result=part.as_string().find(' protected-headers="v1"')
                    if result > 0:
                        HeaderFile.close()
                        HeaderFile = open(outpath +"/"+fle+"_attachments/header.txt", 'w')
                        try:
                            subject, encoding = decode_header(part["Subject"])[0]
                            if isinstance(subject, bytes):
                                # if it's a bytes, decode to str
                                subject = subject.decode(encoding)
                            print("Subject:", subject, file = HeaderFile)
                            # decode email sender
                            From, encoding = decode_header(part.get("From"))[0]
                            if isinstance(From, bytes):
                                From = From.decode(encoding)
                            print("From:", From, file = HeaderFile)
                            # decode email recipient
                            To, encoding = decode_header(part.get("To"))[0]
                            if isinstance(To, bytes):
                                To = To.decode(encoding)
                            print("To:", To, file = HeaderFile)
                        except:
                            pass
        else:
            # extract content type of email
            content_type = msg.get_content_type()
            # get the email body
            body = msg.get_payload(decode=True).decode()
            if content_type == "text/plain":
                # print only text email parts
                print(body, file = BodyFile)
            if content_type == "text/html":
                # if it's HTML, create a new HTML file and open it in browser
                folder_name = outpath
                if not os.path.isdir(folder_name):
                    # make a folder for this email (named after the subject)
                    os.mkdir(folder_name)
                filename = "index.html"
                filepath = os.path.join(folder_name, filename)
                print(body, file = HTMLFile)
    BodyFile.close()
    HTMLFile.close()
    HeaderFile.close()

#
# Variablen und Directorys
#
def main(argv):
    opts, args = getopt.getopt(argv,"h:i:o:p",["help","idir=","odir=","pgppw="])
    outpath=""
    path=""
    pgppw=""

    # gpg = gnupg.GPG()
    # gpg.encoding = 'utf-8'

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print("Usage: --idir=<Verzeichnis mit EML-Dateien> --odir=<Verzeichnis der geparsten Ergebnisse> --pgppw=<Passwort für den PGP-Private-Key> " )
            sys.exit()
        elif opt in ("-i", "--idir"):
            path = arg
        elif opt in ("-o", "--odir"):
            outpath = arg
        elif opt in ("-p", "--pgppw"):
            pgppw = arg

    # Main, Loop über das Verzeichnis
    listing = os.listdir(path)
    for fle in listing:
        if fle.endswith('.eml'):
            os.system("rm -rf "+outpath+"/"+fle+"_attachments/")
            split_mail(path,outpath,fle)
            isExist = os.path.exists(outpath +"/"+fle+"_attachments/PGP.pgp")
            if isExist:
                # decrypt file
                os.system("echo "+pgppw+" | gpg --batch --yes --passphrase-fd 0 --decrypt --output "+outpath +"/"+fle+"_attachments/PGP.txt "+outpath +"/"+fle+"_attachments/PGP.pgp")
                # with open(outpath +"/"+fle+"_attachments/PGP.pgp", 'rb') as f:
                #     if pgppw == "":
                #         decrypted_data = gpg.decrypt_file(f, always_trust=True)
                #     else:
                #         decrypted_data = gpg.decrypt_file(f, always_trust=True, passphrase=pgppw)
                # Prepare
                msg = email.message_from_file(open(path +"/"+ fle))
                From, encoding = email.header.decode_header(msg.get('from'))[0]
                To, encoding = email.header.decode_header(msg.get('to'))[0]
                message_id, encoding = email.header.decode_header(msg.get('Message-ID'))[0]
                Date, encoding = email.header.decode_header(msg.get('Date'))[0]
                with open(outpath +"/"+fle+"_attachments/PGP.eml", "w") as text_file:
                    text_file.write('From: '+From+'\n')
                    text_file.write('To: '+To+'\n')
                    text_file.write('Message-ID: '+message_id+'\n')
                    text_file.write('Date: '+Date+'\n')

                # with open(outpath +"/"+fle+"_attachments/PGP.txt", "w") as text_file:
                #     text_file.write(str(decrypted_data))

                # with open(outpath +"/"+fle+"_attachments/PGP.eml", "a") as text_file:
                #     text_file.write(str(decrypted_data))
                os.system("cat "+outpath +"/"+fle+"_attachments/PGP.txt >> "+outpath +"/"+fle+"_attachments/PGP.eml")

                split_mail(outpath,outpath,fle+"_attachments/PGP.eml")

if __name__ == "__main__":
    if len(sys.argv) == 4:
        main(sys.argv[1:])
    else:
        print("Usage: --idir=<Verzeichnis mit EML-Dateien> --odir=<Verzeichnis der geparsten Ergebnisse> --pgppw=<Passwort für den PGP-Private-Key> ")