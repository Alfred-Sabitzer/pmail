###########################################################################################################################################
#
# Read a specific Email from a given account
#
# https://gist.github.com/morenoh149/f2ed47dc8c57bacf5bc27a899c642d6a
#
###########################################################################################################################################
import imaplib
import sys
import getopt
import email

def main(argv):
    MAIL_IMAP_HOST=""
    MAIL_USER=""
    MAIL_PASSWORD=""
    MAIL_NO=""
    MAIL_FILE=""
    EMAIL_FOLDER = "INBOX"
    opts, args = getopt.getopt(argv,"h:m:u:p:n:f",["help","MAIL_IMAP_HOST=","MAIL_USER=","MAIL_PASSWORD=","MAIL_NO=","MAIL_FILE="])
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print("Usage: --MAIL_IMAP_HOST=Hostname --MAIL_USER=Username --MAIL_PASSWORD=password --MAIL_NO=numberofmail --MAIL_FILE=FQFN")
            sys.exit()
        elif opt in ("-m", "--MAIL_IMAP_HOST"):
            MAIL_IMAP_HOST = arg
        elif opt in ("-u", "--MAIL_USER"):
            MAIL_USER = arg
        elif opt in ("-p", "--MAIL_PASSWORD"):
            MAIL_PASSWORD = arg
        elif opt in ("-n", "--MAIL_NO"):
            MAIL_NO = arg
        elif opt in ("-f", "--MAIL_FILE"):
            MAIL_FILE = arg

    M = imaplib.IMAP4_SSL(MAIL_IMAP_HOST)

    try:
        rv, data = M.login(MAIL_USER, MAIL_PASSWORD)
    except imaplib.IMAP4.error:
        print ("LOGIN FAILED!!! ")
        sys.exit(1)

    print(rv, data)

    rv, data = M.select(EMAIL_FOLDER)
    if rv == 'OK':
        print("Processing mailbox...\n")
        rv, data = M.search(None, "ALL")
        if rv != 'OK':
            print("No messages found!")
            return

        rv, data = M.fetch(MAIL_NO.encode(), '(RFC822)')
        if rv != 'OK':
            print("ERROR getting message", num)
            return

        msg = email.message_from_bytes(data[0][1])
        open(MAIL_FILE, "wb").write(bytes(msg))

        M.close()
    else:
        print("ERROR: Unable to open mailbox ", rv)

    M.logout()

if __name__ == "__main__":
    if len(sys.argv) > 2:
        main(sys.argv[1:])
    else:
        print("Usage: --MAIL_IMAP_HOST=Hostname --MAIL_USER=Username --MAIL_PASSWORD=password --MAIL_NO=numberofmail --MAIL_FILE=FQFN")
